<?php // apertura
// Variables en PHP
$variable = "Hola Mundo";
$var1 = 1; // numero
// arreglo
$var2 = ['Edson','Broly','Zaqueo'];
// string
$var3 = "Hola";
$var4 = "Mundo"; //string
$numeros = [5,2,6,1,8,7,"hola",false,0]; // aregglo con diferentes tipos de datos
// cierre ?>

<!DOCTYPE html>
<html lang="es">
<head>
<title>Taller PHP</title>
<meta charset="utf-8" />
</head>
 
<body>
<?php echo $variable ?>
<br>
<?php echo $var1 ?>
<br>
<?php
echo $var2[2];
?>
<br>
<?php
//concatenar variables
echo $var3." ".$var4;
echo "<br>";
$var5 =$var3." Zaqueo - ".$var1;
echo $var5;

echo "<hr>";
echo $numeros[3]."x".$numeros[5]." = ".($numeros[3]*$numeros[5])."<br>";
echo $numeros[1]."/".$numeros[0]." = ".($numeros[1]/$numeros[0])."<br>";
echo $numeros[3]."+".$numeros[4]." = ".($numeros[3]+$numeros[4])."<br>";
echo $numeros[5]."-".$numeros[5]." = ".($numeros[5]-$numeros[5])."<br>";
echo "<br>";
print_r($numeros); // imprimir arreglos
echo "<hr>";
for($i =0; $i<=8;$i++)
{
	echo $numeros[$i]." ";
}
echo "<br>";//salto de linea

foreach($numeros as $key => $numero)
{
echo "[".$key."]".$numero." ";
}
echo "<br>";
 if ($numeros[7])
{
echo "es verdadero";

}
else 
{
echo "nada que ver";
}


?>
</body>
</html>
